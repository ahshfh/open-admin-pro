/**
 * 项目默认配置项
 * primaryColor - 默认主题色, 如果修改颜色不生效，请清理 localStorage
 * navTheme - sidebar theme ['dark', 'light'] 两种主题
 * colorWeak - 色盲模式
 * layout - 整体布局方式 ['sidemenu', 'topmenu'] 两种布局
 * fixedHeader - 固定 Header : boolean
 * fixSiderbar - 固定左侧菜单栏 ： boolean
 * autoHideHeader - 向下滚动时，隐藏 Header : boolean
 * contentWidth - 内容区布局： 流式 |  固定
 *
 * storageOptions: {} - Vue-ls 插件配置项 (localStorage/sessionStorage)
 *
 */
export default {
  name: 'OpenCloud',
  appId: process.env.VUE_APP_ID,
  appApiKey: process.env.VUE_APP_API_KEY,
  appSecretKey: process.env.VUE_APP_SECRET_KEY,
  production: process.env.NODE_ENV === 'production' && process.env.VUE_APP_PREVIEW !== 'true',
  apiUrl: process.env.VUE_APP_API_BASE_URL,
  // vue-ls options
  storageOptions: {
    namespace: 'pro__', // key prefix
    name: 'ls', // name variable Vue.[ls] or this.[$ls],
    storage: 'local' // storage name session, local, memory
  },
  primaryColor: '#2d8cf0', // 主色彩
  navTheme: 'dark', // 导航主题色 dark or light
  layout: 'sidemenu', // 导航布局: sidemenu or topmenu
  contentWidth: 'Fixed', // 内容区域宽度: Fluid or Fixed, only works when layout is topmenu
  fixedHeader: true, // 固定 Header
  fixSiderbar: true, // 固定侧边菜单
  autoHideHeader: false, // 下滑时隐藏 Header
  colorWeak: false,
  multiTab: false

}
